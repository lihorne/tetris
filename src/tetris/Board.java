package tetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import javax.swing.JLabel;
import javax.swing.JPanel; // JPanel for statusbar
import javax.swing.Timer; // Timer to pace entire runtime

import tetris.Shape.Tetrominoes; // Uses the shapes defined in Shape.java

public class Board extends JPanel implements ActionListener {

    /* Self explanatory variables and constants*/
    final int BOARD_WIDTH = 10; // Number of blocks in a row and column
    final int BOARD_HEIGHT = 22;
    final int GAME_LENGTH = 120; // length of game
    Timer timer; // Sets the timer for game mechanics (NOT THE GAME TIMER)
    boolean isFallingFinished = false;
    boolean isStarted = false;
    boolean isPaused = false;
    public static int numLinesRemoved = 0;
    public static int score = 0;
    public static int bestScore = 0;
    public static boolean gameTimedOut = false;
    int curX = 0;
    int curY = 0;
    JLabel statusbar;
    Shape curPiece;
    Tetrominoes[] board;
    Shape ghostPiece;
    int ghostX = 0;
    int ghostY = 0;
    long time, start;
    long timeLost[] = new long[1000];
    long startOfPause, endOfPause;
    int timesPaused = 0;
    String[] temp = new String[0];

    public Board(Tetris parent) {
        setFocusable(true); // From now, the board has the keyboard input.
        curPiece = new Shape(); // Variable of the current piece that is falling
        ghostPiece = new Shape(); // Variable of the ghost piece (mostly for readability of code)
        timer = new Timer(400, this); // Defines the timer
        timer.start(); // Start the timer
        start = System.nanoTime(); // Defines the exact time the application started accurate to 10^-9 seconds
        setBackground(new Color(37, 37, 37));
        statusbar = parent.getStatusBar();
        board = new Tetrominoes[BOARD_WIDTH * BOARD_HEIGHT]; // Defines the number of blocks on the board
        addKeyListener(new TAdapter()); // Adds a listener input
        clearBoard();
    }

    /* METHODS DIRECTLY RELATED TO THE MECHANICS OF THE ACTUAL GAME MOTION */
    ////////////////////////////////////////////////////////////////////////
    int squareWidth() {
        return (int) getSize().getWidth() / BOARD_WIDTH; // Gets the width of a single square
    }

    int squareHeight() {
        return (int) getSize().getHeight() / BOARD_HEIGHT; // Height
    }

    Tetrominoes shapeAt(int x, int y) { // Returns the shape that exists at some coordinate on the board
        return board[(y * BOARD_WIDTH) + x];
    }

    private void ghost() { // Function for finding the location of a ghost piece
        int newY = curY;
        while (newY > 0) {
            if (!tryMove(curPiece, curX, newY - 1, true)) {
                break;
            }
            newY--;
        }
    }

    private boolean tryMove(Shape newPiece, int newX, int newY, boolean ghost) {
        /* TRIES to move a piece to a new location based on a function */
        for (int i = 0; i < 4; ++i) {
            int x = newX + newPiece.x(i);
            int y = newY - newPiece.y(i);
            if (x < 0 || x >= BOARD_WIDTH || y < 0 || y >= BOARD_HEIGHT) {
                return false;
            }
            if (shapeAt(x, y) != Tetrominoes.NoShape) {
                return false;
            }
        }

        if (!ghost) { // If it is a real piece, set new coordinates for game use
            curPiece = newPiece;
            curX = newX;
            curY = newY;
            repaint();
            return true;
        } else { // If it is a ghost piece, set location of the ghost piece
            ghostPiece = newPiece;
            ghostX = newX;
            ghostY = newY;
            repaint();
            return true;
        }
    }

    private void removeFullLines() {
        /* Removes a cleared row and drops pieces above it */
        int numFullLines = 0;

        for (int i = BOARD_HEIGHT - 1; i >= 0; --i) {
            boolean lineIsFull = true;

            for (int j = 0; j < BOARD_WIDTH; ++j) {
                if (shapeAt(j, i) == Tetrominoes.NoShape) {
                    lineIsFull = false;
                    break;
                }
            }

            if (lineIsFull) {
                numFullLines++;
                for (int k = i; k < BOARD_HEIGHT - 1; ++k) {
                    for (int j = 0; j < BOARD_WIDTH; ++j) {
                        board[(k * BOARD_WIDTH) + j] = shapeAt(j, k + 1);
                    }
                }
            }
        }

        switch (numFullLines) { // Score system
            case 1:
                score += 100;
                break;
            case 2:
                score += 300;
                break;
            case 3:
                score += 500;
                break;
            case 4:
                score += 1000;
                break;
        }

        if (numFullLines > 0) {
            numLinesRemoved += numFullLines;
            isFallingFinished = true;
            curPiece.setShape(Tetrominoes.NoShape);
            repaint();
        }

    }

    private void drawSquare(Graphics g, int x, int y, Tetrominoes shape, boolean ghost) {

        // Sets the colors to be used within each shape
        //     { NoShape, ZShape, SShape, LineShape,
        //     TShape, SquareShape, LShape, MirroredLShape, GHOST };
        Color colors[] = {new Color(0, 0, 0), new Color(246, 61, 41),
            new Color(18, 170, 44), new Color(0, 212, 212),
            new Color(191, 0, 255), new Color(255, 226, 0),
            new Color(255, 162, 0), new Color(0, 122, 255), new Color(84, 84, 84)
        };


        Color color = colors[shape.ordinal()];

        if (ghost) {
            color = colors[8];
        }

        // Sets the color of the main box
        g.setColor(color);
        g.fillRect(x + 1, y + 1, squareWidth() - 2, squareHeight() - 2);

        // Sets the color of a brighter outline
        g.setColor(color.brighter());
        g.drawLine(x, y + squareHeight() - 1, x, y);
        g.drawLine(x, y, x + squareWidth() - 1, y);

        // Sets the color of a darker outline
        g.setColor(color.darker());
        g.drawLine(x + 1, y + squareHeight() - 1,
                x + squareWidth() - 1, y + squareHeight() - 1);
        g.drawLine(x + squareWidth() - 1, y + squareHeight() - 1,
                x + squareWidth() - 1, y + 1);

    }

    private void dropDown() {
        /* If space is hit then the piece is dropped to it's new location at its bottom most point */
        int newY = curY;
        while (newY > 0) {
            if (!tryMove(curPiece, curX, newY - 1, false)) {
                break;
            }
            newY--;
        }
        pieceDropped();
    }

    private void oneLineDown() {
        /* Simple one line down function */
        if (!tryMove(curPiece, curX, curY - 1, false)) {
            pieceDropped();
        }
    }

    private void clearBoard() {
        /* Clears the entire board */
        for (int i = 0; i < BOARD_HEIGHT * BOARD_WIDTH; ++i) {
            board[i] = Tetrominoes.NoShape;
        }
    }

    private void pieceDropped() {
        /* Occurs when the piece is in its final position */
        score += 10;
        for (int i = 0; i < 4; ++i) {
            int x = curX + curPiece.x(i);
            int y = curY - curPiece.y(i);
            board[(y * BOARD_WIDTH) + x] = curPiece.getShape();
        }

        removeFullLines();

        if (!isFallingFinished) {
            newPiece();
        }
    }

    private void newPiece() {
        // Gets a new piece to be dropped
        curPiece.setRandomShape();
        curX = BOARD_WIDTH / 2;
        curY = BOARD_HEIGHT - 1 + curPiece.minY();

        if (!tryMove(curPiece, curX, curY, false)) {
            gameEnded(); // Game is over when the new piece can't exist
        }
    }

    /* METHODS NECESSARY FOR OTHER FEATURES AND MECHANICS NOT INDEPENDANT TO THE GAME MOTIONS*/
    //////////////////////////////////////////////////////////////////////////////////////////
    public void actionPerformed(ActionEvent e) { // action method
        if (Tetris.buttonCounter < 1) { // New game hasn't been pressed initially
            return;
        }

        checkMenu(); // Check to see if a menu button has been pressed
        updateTime(); // Update the time

        if (time < GAME_LENGTH) { // if the game still has time, perform normal game functions
            isStarted = true;
            if (isFallingFinished) {
                isFallingFinished = false;
                newPiece();
            } else {
                oneLineDown();
            }
        } else { // Time is up, game is ended
            timer.stop();
            isStarted = false;
            gameEnded();
        }

    }

    public void start() { // Starts the game
        if (isPaused || !isStarted) {
            return;
        }
        isStarted = true;
        isFallingFinished = false;
        numLinesRemoved = 0;
        clearBoard();
        newPiece();
        timer.start();
        start = System.nanoTime();
    }

    private void pause() { // Pauses the game

        if (!isStarted) { // If the game hasn't started, disregards method
            return;
        }

        if (!isPaused) {
            startOfPause = System.nanoTime();
        } else {
            timesPaused++;
            endOfPause = System.nanoTime();
            timeLost[timesPaused] = (endOfPause) - (startOfPause);
        }

        isPaused = !isPaused; // If it is paused, now it is not paused ('P' and 'p' both pause and unpause the game)
        if (isPaused) {
            timer.stop(); // Stops the timer if paused
            statusbar.setText("paused");
        } else {
            timer.start();
        }
        repaint();

    }

    public void paint(Graphics g) { // Paints the board
        super.paint(g);
        int y = 0;
        int x = 0;
        Dimension size = getSize();
        // boardTop is the number of pixels leftover at the top of the board
        int boardTop = (int) size.getHeight() - BOARD_HEIGHT * squareHeight(); // Determines the unused space at the top of the board

        for (int i = 0; i < BOARD_HEIGHT; ++i) {
            for (int j = 0; j < BOARD_WIDTH; ++j) {
                Tetrominoes shape = shapeAt(j, BOARD_HEIGHT - i - 1);
                if (shape != Tetrominoes.NoShape) {
                    drawSquare(g, 0 + j * squareWidth(),
                            boardTop + i * squareHeight(), shape, false);
                }
            }
        }

        if (curPiece.getShape() != Tetrominoes.NoShape) {

            /* Draws each indidivual block of a GHOST of the current shape */
            for (int i = 0; i < 4; ++i) {
                ghost();
                x = ghostX + ghostPiece.x(i);
                y = ghostY - ghostPiece.y(i);
                drawSquare(g, x * squareWidth(),
                        boardTop + (BOARD_HEIGHT - y - 1) * squareHeight(),
                        ghostPiece.getShape(), true);
            }

            /* Draws each individual block of a shape */
            for (int i = 0; i < 4; ++i) {
                x = curX + curPiece.x(i);
                y = curY - curPiece.y(i);
                drawSquare(g, x * squareWidth(),
                        boardTop + (BOARD_HEIGHT - y - 1) * squareHeight(),
                        curPiece.getShape(), false);
            }
        }
        checkMenu();
    }

    public void checkMenu() {
        // Code to check up on menu clicks
        if (Tetris.viewScores) {
            Tetris.viewScores = false;
            if (!isPaused) {
                pause();
            }
            Leaderboard.main(temp);
        }

        if (Tetris.newGame && Tetris.buttonCounter >= 1) {
            Tetris.newGame = false;
            isPaused = false;
            timer.start();
            reset();
            start();
        }

        if (Tetris.openHelp) {
            Tetris.openHelp = false;
            try {
                Runtime.getRuntime().exec("notepad readme.txt");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void gameEnded() {
        if (time < GAME_LENGTH && !gameTimedOut) {   // If the top is reached
            statusbar.setText("Game Over");
            isStarted = false;
            
        } else { // If time runs out
            statusbar.setText("Time's Up                          " +
                    "Score: " + score);
            if (score >= bestScore) {
                bestScore = score;
                gameTimedOut = true;
                Leaderboard.main(temp);
            }

        }
    }

    public void reset() {
        // Simple resets important variables
        isFallingFinished = true;
        for (int i = 0; i <=
                timesPaused; i++) {
            timeLost[i] = 0;
        }

        start = System.nanoTime();
        clearBoard();

        score = 0;
        numLinesRemoved = 0;
    }

    public void updateTime() { // this method is used to set the timer
        long totalTimeLost = 0; // variable that keeps track of time lost due to pauses
        for (int i = 0; i <=
                timesPaused; i++) { // this for loop adds all the lost times due to pauses
            totalTimeLost += timeLost[i];
        }

        if (!isPaused || isStarted) {
            time = (System.nanoTime() - start - totalTimeLost) / 1000000000; // calculate the total time the game has been running
            statusbar.setText("Lines: " + String.valueOf(numLinesRemoved) +
                    "           Score: " + score +
                    "           Time Left: " + (GAME_LENGTH - (time))); //output to the statusbar
        }
    }

    class TAdapter
            extends KeyAdapter { // class for input

        public void keyPressed(KeyEvent e) { // sets a variable for the input

            if (!isStarted || curPiece.getShape() == Tetrominoes.NoShape) { // if the game hasn't started or a no piece is falling
                return;                                                     // game doesnt read input
            }

            int keycode = e.getKeyCode(); // variable for input

            if (keycode == 'p' || keycode == 'P') { // if the game is paused
                /* If Statement gathers the time taken for a pause */
                pause();
                return;
            }

            if (isPaused) { // If the game is paused, method doesn't read input
                return;
            }

            switch (keycode) { // Switch statement for various different commands
                case KeyEvent.VK_LEFT:
                    tryMove(curPiece, curX - 1, curY, false); // move left
                    break;
                case KeyEvent.VK_RIGHT:
                    tryMove(curPiece, curX + 1, curY, false); // move right
                    break;
                case KeyEvent.VK_UP:
                    tryMove(curPiece.rotateRight(), curX, curY, false); // rotate
                    break;
                case KeyEvent.VK_DOWN: // if the user wants to speed up the falling process
                    oneLineDown();
                    break;
                case KeyEvent.VK_SPACE: // if the user wants to drop the block now
                    dropDown();
                    break;
                case 'r': // reset
                    reset();
                    break;
                case 'R': // reset
                    reset();
                    break;
                case 'X': // rotate right
                    tryMove(curPiece.rotateRight(), curX, curY, false);
                    break;
                case 'Z': // rotate left
                    tryMove(curPiece.rotateLeft(), curX, curY, false);
                    break;
            }

        }
    }
}
