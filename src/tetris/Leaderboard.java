package tetris;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Leaderboard extends JPanel {

    public static int score, numLinesRemoved, numberOfEntries;
    public static String username;
    public static ArrayList<ArrayList<String>> records = new ArrayList<ArrayList<String>>();
    String[][] info;

    public Leaderboard() {
        super(new GridLayout(1, 0));

        score = Board.bestScore; // Gets the score
        numLinesRemoved = Board.numLinesRemoved; // lines removed

        if (Board.gameTimedOut) { // The game only adds scores that are earned by timing out, NOT BY HITTING THE TOP
            Board.gameTimedOut = false;
            addScore();
        }
        
        readScore(); // Read the scores

        /* Store the scores in an array */
        info = new String[numberOfEntries][3];

        for (int x = 0; x < numberOfEntries; x++) {
            for (int y = 0; y <= 2; y++) {
                info[x][y] = records.get(x).get(y);
            }
        }

        sortScore(); // Sort the scores

        String[] columnNames = {"Name", "Lines Cleared", "Score"};

        Object[][] data = info;

        /* Set up the leaderboard table */
        final JTable table = new JTable(data, columnNames);
        table.setPreferredScrollableViewportSize(new Dimension(400, 400));
        table.setFillsViewportHeight(true);

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this panel.
        add(scrollPane);
    }

    public void addScore() {
        // Adds the score the user just earned
        try {
            String filepath = "highscores.xml";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance(); // this section builds the paths necessary for modificaition
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filepath);

            doc.getDocumentElement().normalize();

            Node highscores = doc.getElementsByTagName("highscores").item(0);

            Element user = doc.createElement("user");

            Element name = doc.createElement("username");
            name.appendChild(doc.createTextNode(username));
            user.appendChild(name);

            Element lines = doc.createElement("linescleared");
            lines.appendChild(doc.createTextNode(Integer.toString(numLinesRemoved)));
            user.appendChild(lines);

            Element scoreE = doc.createElement("score");
            scoreE.appendChild(doc.createTextNode(Integer.toString(score)));
            user.appendChild(scoreE);

            highscores.appendChild(user);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(filepath));
            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void readScore() {
        // Reads every score in highscores.xml
        records.clear();
        try {
            String filepath = "highscores.xml";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance(); // this section builds the paths necessary for modificaition
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filepath);

            doc.getDocumentElement().normalize();
            NodeList listOfUsers = doc.getElementsByTagName("user");

            numberOfEntries = listOfUsers.getLength();

            for (int s = 0; s < listOfUsers.getLength(); s++) {
                ArrayList<String> userRecord = new ArrayList<String>();
                Node firstUserNode = listOfUsers.item(s);

                if (firstUserNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element firstUserElement = (Element) firstUserNode;

                    NodeList nameList = firstUserElement.getElementsByTagName("username");
                    Element nameElement = (Element) nameList.item(0);
                    NodeList textNList = nameElement.getChildNodes();
                    userRecord.add(((Node) textNList.item(0)).getNodeValue().trim());

                    NodeList linesClearedList = firstUserElement.getElementsByTagName("linescleared");
                    Element linesClearedElement = (Element) linesClearedList.item(0);
                    NodeList textLCList = linesClearedElement.getChildNodes();
                    userRecord.add(((Node) textLCList.item(0)).getNodeValue().trim());

                    NodeList scoreList = firstUserElement.getElementsByTagName("score");
                    Element scoreElement = (Element) scoreList.item(0);
                    NodeList textScoreList = scoreElement.getChildNodes();
                    userRecord.add(((Node) textScoreList.item(0)).getNodeValue().trim());

                    records.add(userRecord);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    int partition(String a[][], int left, int right) {
        int i = left, j = right;
        int temp;
        String tempName, tempLine;

        int pivot = Integer.parseInt(a[(left + right) / 2][2]); // setting the pivot

        while (i <= j) {
            while (Integer.parseInt(a[i][2]) > pivot) { // finding the partition
                i++;
            }
            while (Integer.parseInt(a[j][2]) < pivot) { // finding the partition
                j--;
            }
            if (i <= j) { // swapping the terms according to order and the pivot
                temp = Integer.parseInt(a[i][2]);
                tempName = a[i][0];
                tempLine = a[i][1];
                a[i][2] = a[j][2]; // swap
                a[i][1] = a [j][1];
                a[i][0] = a[j][0];
                a[j][2] = Integer.toString(temp);
                a[j][1] = tempLine;
                a[j][0] = tempName;
                i++; // continuing
                j--;
            }
        }
        return i; // return the new value
    }

    void quickSort(String a[][], int left, int right) {
        int index = partition(a, left, right); // new partition
        if (left < index - 1) {
            quickSort(a, left, index - 1); // start from left to new index
        }
        if (index < right) {
            quickSort(a, index, right); // start from new index to right
        }
    }

    public void sortScore(){ // Sorts the scores in descending order
        quickSort(info, 0, numberOfEntries - 1);
    }

    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("High Scores");

        //Create and set up the content pane.
        Leaderboard newContentPane = new Leaderboard();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                createAndShowGUI();
            }
        });
    }
}
