package tetris;

import java.awt.BorderLayout;

import javax.swing.*;
import java.awt.event.*;
import java.awt.event.ActionListener;

public class Tetris extends JFrame {

    JLabel statusbar = new JLabel(); // status bar on bottom of game screen
    
    JFrame nameinput = new JFrame("Name Input"); // name input window
    JTextField textField = new JTextField(20);

    public static final int WINDOW_WIDTH = 296; // size of game window
    public static final int WINDOW_HEIGHT = 671;

    public static boolean newGame = false;   // various booleans for game operations
    public static boolean viewScores = false;
    public static boolean pauseGame = false;
    public static boolean openHelp = false;
    public static boolean nameEntered = false;
    public static int buttonCounter = 0; // used to check for the first time NEW GAME is pressed

    public Tetris() {
        /* Get the username of the user and opens the main Tetris game */
        getUsername();
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setTitle("Tetris");
        add(statusbar, BorderLayout.SOUTH);
        menuBar();
        Board board = new Board(this);
        add(board);
        board.start();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public JLabel getStatusBar() {
        return statusbar;
    }

    public void getUsername() {
        /* Method for opening the input window and collecting the username */

        JLabel label = new JLabel("   Name: ");
        JLabel introLabel = new JLabel("<HTML><CENTER>Welcome to TETRIS Alpha by Liam Horne<BR><BR>" +
                "Please enter your name in the textbox below and click SUBMIT. <BR>" +
                "To start a new game, hit FILE then NEW GAME in the game frame.<BR> " +
                "For info about the game, hit HELP then ABOUT.<BR> <BR>  </CENTER></HTML>");
        JLabel bottomLabel = new JLabel("<HTML> <BR> <BR> </HTML>");
        JPanel panel = new JPanel();
        JButton button = new JButton("Submit");

        panel.add(button);

        nameinput.setSize(400, 220);
        nameinput.setLocationRelativeTo(null);

        textField.setLocation(60, 22);
        textField.setSize(250, 25);

        button.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        Leaderboard.username = textField.getText();
                        nameEntered = true;
                        nameinput.dispose();
                    }
                });

        nameinput.add(bottomLabel, BorderLayout.SOUTH);
        nameinput.add(introLabel, BorderLayout.NORTH);
        nameinput.add(textField);
        nameinput.add(label, BorderLayout.WEST);
        nameinput.add(panel, BorderLayout.EAST);
        nameinput.setVisible(true);
        nameinput.setAlwaysOnTop(true);
        nameinput.setResizable(false);
    }

    public void menuBar() { 
        // This method sets up the menu bar that has options for a new game, high scores, and help
        JMenu file = new JMenu("File");
        file.setMnemonic('F');

        JMenu help = new JMenu("Help");
        file.setMnemonic('H');

        JMenuItem newItem = new JMenuItem("New Game");
        newItem.setMnemonic('N');
        file.add(newItem);

        JMenuItem viewItem = new JMenuItem("High Scores");
        viewItem.setMnemonic('H');
        file.add(viewItem);

        JMenuItem helpItem = new JMenuItem("About");
        helpItem.setMnemonic('H');
        help.add(helpItem);

        newItem.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        if (Tetris.nameEntered) {
                            newGame = true;
                            buttonCounter++;
                        } else {
                            nameinput.setVisible(true);
                        }
                    }
                });

        viewItem.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        viewScores = true;
                    }
                });

        helpItem.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        openHelp = true;
                    }
                });


        JMenuBar bar = new JMenuBar();
        setJMenuBar(bar);
        bar.add(file);
        bar.add(help);
    }

    public static void main(String[] args) {
        tetris.Tetris game = new tetris.Tetris();
        game.setLocationRelativeTo(null);
        game.setVisible(true);
    }
}
