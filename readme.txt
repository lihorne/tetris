Welcome to Tetris Alpha Release 1.0.0 by Liam Horne using Java

To begin the game, enter your name in the box that launches and
hit 'Submit'. Then, click on FILE and NEW GAME.

In Tetris, shapes called Tetrominoes fall down the screen one line
at a time. A Tetrominoe is a shape consisting of four blocks.
The goal of the game is to manipulate these shapes by moving them
left and right and rotating them as they fall down the screen to
create rows of only blocks (no gaps). Points are awarded not only
for rows (or lines) cleared, but also for number of rows cleared
by a single move (see POINT SYSTEM).

///////////////// CONTROLS //////////////////

LEFT ARROW KEY - Move left
RIGHT ARROW KEY - Move right
UP ARROW KEY - Rotate clockwise
DOWN ARROW KEY - Move down one line

SPACE BAR - Drop piece to bottom position

'R' or 'r' - Quickly reset game (FILE > NEW GAME will do the same)
'P' or 'p' - Pause or unpause the game

'X' or 'x' - Rotate clockwise
'Z' or 'z' - Rotate counterclockwise

//////////////// POINT SYSTEM ///////////////

Block dropped - 10 points
1x Line Cleared - 100 Points
2x Line Cleared Combo - 300 Points
3x Line Cleared Combo - 500 Points
4x Line Cleared Combo - 1000 Points